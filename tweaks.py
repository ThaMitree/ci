import pandas as pd
from data import dataset, datasetdefined
import datetime as dt
import ast

def daytimeTweak(df):
    today = dt.datetime.now().strftime('%A')
    # print(today)
    # days = {'Friday' : 11}
    # print(today in days)
    df = df.dropna()
    # df = df.head(1)
    for item in df.hours:
        print(item)
        print('Monday' in item)
    return df.hours

def attributesFilter(df):
    length = len(df)
    df_attributes = df['attributes'].dropna()
    attributeList = []
    for i in range(length):
        try:
            for attribute in df_attributes[i]:
                itemInAttribute = df_attributes[i][attribute]
                if itemInAttribute == 'True':
                    attributeList.append([df.business_id[i], attribute])
                elif attribute == 'Ambience':
                    mydict = ast.literal_eval(itemInAttribute)
                    for item in mydict:
                        if mydict[item] == True:
                            attributeList.append([df.business_id[i], item])
                elif attribute == 'BusinessParking':
                    mydict = ast.literal_eval(itemInAttribute)
                    for item in mydict:
                        if mydict[item] == True:
                            attributeList.append([df.business_id[i], 'BusinessParking'])
                            break
                # elif itemInAttribute != 'False':
                    # print(attribute, itemInAttribute)
        except:
            next
    returnDf = pd.DataFrame(attributeList, columns=['business_id', 'attributes'])
    returnDf = returnDf.pivot_table(index='business_id', columns='attributes', aggfunc='size', fill_value=0)
    return returnDf


# print(attributesFilter(dataset('ajax', 'business', 'all')))
# print(daytimeTweak(dataset('ajax', 'business', 'all')))