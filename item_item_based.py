import pandas as pd
import numpy as np
from scipy.spatial import distance

# create similarity matrix
def create_similarity_matrix_cosine(matrix):
    index = pd.Series(matrix.index)
    df = pd.DataFrame(index.apply(lambda x: index.apply(lambda y: cosine_similarity(matrix, x, y))))
    return df.set_index(index).set_axis(index, axis=1, inplace=False)

# cosine similarity function that handles nan input (and isn't very optimized)
def cosine_similarity(matrix, id1, id2):
    features1 = matrix.loc[id1].notna()
    features2 = matrix.loc[id2].notna()
    return 1 - distance.cosine(features1, features2)
