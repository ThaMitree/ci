import os
import pandas as pd

def read_json(city, type):
    path_to_json = 'data/' + city
    file_with_extension = type + '.json'
    return pd.read_json(os.path.join(path_to_json, file_with_extension), lines=True)

def datasetdefined(type, quantity):
    return dataset('las vegas', type, quantity)

def dataset(city, type, quantity):
    type = type.lower()
    if type == 'business':
        return business(read_json(city, type), quantity)
    elif type == 'checkin':
        return checkin(read_json(city, type), quantity)
    elif type == 'review':
        return review(read_json(city, type), quantity)
    elif type == 'tip':
        return tip(read_json(city, type), quantity)
    elif type == 'user':
        return user(read_json(city, type), quantity)
    else:
        raise Exception('Type is unknown. Known types: business, checkin, review, tip, user. The value of type was: {}'.format(type))

def business(json_file, quantity):
    if quantity == 'all':
        quantity = len(json_file)
    return json_file.head(quantity)

def checkin(json_file, quantity):
    if quantity == 'all':
        quantity = len(json_file)
    return json_file.head(quantity)

def review(json_file, quantity):
    if quantity == 'all':
        quantity = len(json_file)
    return json_file.head(quantity)

def tip(json_file, quantity):
    if quantity == 'all':
        quantity = len(json_file)
    return json_file.head(quantity)

def user(json_file, quantity):
    if quantity == 'all':
        quantity = len(json_file)
    return json_file.head(quantity)
