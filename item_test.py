from data import *
from item_item_based import *
from shared import *
import pandas as pd
import numpy as np


# create dataframe for creating utility and to_predict
df_review = dataset('ajax', 'review', 'all')

# remove duplicates
df_undup = remove_duplicates(df_review)

# create utility_matrix
um = pivot_ratings(df_undup)

# mean-center utility matrix (tbd)
# um = um.subtract(um.mean())

# prepare dataFrame for predictions
ratings = to_predict(df)

# create similarity_matrix
sm = create_similarity_matrix_cosine(um)

# create dataframe with ratings
df_predicted_rating = predict_ratings(sm, um, ratings)
# print(df_predicted_rating)

# see how many ratings are not predicted 0
df_filtered = df_predicted_rating[df_predicted_rating['predicted rating'] > 0]
# print('There are', len(df_predicted_rating), "possible predictions")
# print("There are",len(df_filtered), "actually predicted")

# Example
predictions = predict_for_user('ltEfVC92J-sBgpIGyXCoZw', ratings, sm))
df = pd.DataFrame(predictions), columns=['business_id', 'predicted_rating'])
print(df)
