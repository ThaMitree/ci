import pandas as pd
import numpy as np

# create similarity matrix
def create_similarity_matrix_jaccard(matrix):
    m11 = matrix @ matrix.T
    m10 = pd.DataFrame(matrix.sum(axis=1).values + np.zeros(m11.shape), index=m11.index, columns=m11.index)
    m01 = m10.T
    return m11 / (m01 + m10 - m11)
