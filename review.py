from data import *
from content_based import *
from item_item_based import *
from scipy.spatial import distance
from shared import *
from tweaks import *
from collections import defaultdict
import pandas as pd
import numpy as np
import time

test = {'mse': False, 'user': True, 'city': 'ajax', 'userId': 'ltEfVC92J-sBgpIGyXCoZw', 'print_pred_df': False}

def split_data(data, d = 0.75):
    """Split data in a training and test set.

    Arguments:
    data -- any dataFrame.
    d    -- the fraction of data in the training set
    """
    np.random.seed(seed=5)
    mask_test = np.random.rand(data.shape[0]) < d
    return data[mask_test], data[~mask_test]

# get data for both algorithms
df = dataset(test['city'], 'review', 'all')
df_bus = dataset(test['city'], 'business', 'all')
df_atr = attributesFilter(df_bus)
df_undup = remove_duplicates(df)


# utility matrix for both
um = pivot_ratings(df_undup)

# similarity matrix for both
sm_cb = create_similarity_matrix_jaccard(df_atr)
sm_iib = create_similarity_matrix_cosine(um)

# create dataframes with predicted ratings
df_pred_cb = predict_ratings(sm_cb, um, to_predict(df))
df_pred_iib = predict_ratings(sm_iib, um, to_predict(df))
if test['print_pred_df']:
    print(df_pred_cb, df_pred_iib)

def mse(predicted_ratings):
    """Computes the mean square error between actual ratings and predicted ratings

    Arguments:
    predicted_ratings -- a dataFrame containing the columns stars and predicted rating
    """
    return ((predicted_ratings['stars'] - predicted_ratings['predicted rating']) ** 2).mean()

def random_predict(df):
    df2 = to_predict(df.copy())
    df2["predicted rating"] = np.random.uniform(1.0, 5.0, df2.shape[0])
    return df2

def mean_predict(df):
    df2 = to_predict(df.copy())
    df2["predicted rating"] = np.mean(df2["stars"])
    return df2

if test['mse']:
    df_pred_random = random_predict(df)
    df_pred_mean = mean_predict(df)
    print("mse for content based: ", mse(df_pred_cb))
    print("mse for item-item based: ", mse(df_pred_iib))
    print("mse for random: ", mse(df_pred_random))
    print("mse for mean prediction: ", mse(df_pred_mean))

def variation(df_rec):
    categories = set()
    for item in df_rec[0]:
        categories |= set(str(df_bus[df_bus['business_id'] == item]["categories"].values[0]).split(', '))
    return len(categories)/df_rec.shape[0]

def relevance(df_rec, df_rev):
    cat_rec = []
    for item in df_rec[0]:
        for cat in str(df_bus[df_bus['business_id'] == item]["categories"].values[0]).split(', '):
            cat_rec.append((cat, item))
    dict_rec = defaultdict(list)
    for k, v in cat_rec:
        dict_rec[k].append(v)
    cat_vis = set()
    for item in df_rev[df_rev['user_id'] == test['userId']]['business_id']:
        cat_vis |= set(str(df_bus[df_bus['business_id'] == item]["categories"].values[0]).split(', '))
    cat_rev = set(dict_rec.keys()) & cat_vis
    relevant = set()
    for cat in cat_rev:
        relevant |= set(dict_rec[cat])
    return len(relevant)/df_rec.shape[0]

if test['user']:
    cb_test = pd.DataFrame(predict_user_weighted(test['userId'], df, sm_cb)).sort_values(by=1, ascending=False).drop_duplicates().dropna()
    iib_test = pd.DataFrame(predict_user_weighted(test['userId'], df, sm_iib)).sort_values(by=1, ascending=False).drop_duplicates().dropna()
    cb_test = cb_test.head(20)
    print(cb_test)
    print("\ncb Variatie:", variation(cb_test))
    print("cb Relevantie:", relevance(cb_test, df), "\n")
    iib_test = iib_test.head(20)
    print(iib_test)
    print("\niib Variatie:", variation(iib_test))
    print("iib Relevantie:", relevance(iib_test, df))
