from data import *
from tweaks import *
from content_based import *
from shared import *
import pandas as pd
import numpy as np


# create dataframe for creating utility and to_predict
df_business = dataset('ajax', 'review', 'all')

# create dataframe to create similarity
df_atrributes = attributesFilter(dataset('ajax', 'business', 'all'))

# create similarity
similarity_matrix = create_similarity_matrix_jaccard(df_atrributes)
# print(similarity_matrix['V84YoNmnXlp3EK50jWSFpA']['zoJZ_LgSBq3YznjrzXkG3Q'])
# create utility
df_business_no_dub = remove_duplicates(df_business)
utility_matrix = pivot_ratings(df_business_no_dub)

# create to_predict
df_ratings = to_predict(df_business)

# function to predict ratings
df_predicted_rating = predict_ratings(similarity_matrix, utility_matrix, df_ratings)

# see how many ratings are not predicted 0
df_filtered = df_predicted_rating[df_predicted_rating['predicted rating'] > 0]


# create df for reviews
# df_review = (dataset('ajax', 'review', 'all'))
# print(df_review[['user_id', 'business_id']])


# (predict_for_user('ltEfVC92J-sBgpIGyXCoZw', df_business ,similarity_matrix))
# df = pd.DataFrame((predict_for_user('ltEfVC92J-sBgpIGyXCoZw', df_business ,similarity_matrix)), columns=['business_id', 'predicted_rating'])
# print(df)


# nieuwe functie
df_gewogen = pd.DataFrame(predict_user_weighted('ltEfVC92J-sBgpIGyXCoZw', df_business, similarity_matrix))
df_gewogen = df_gewogen.sort_values(by=1, ascending=False)
df_gewogen = df_gewogen.drop_duplicates()
print(df_gewogen.head())
