import pandas as pd
import numpy as np

# create to_predict dataframe
def to_predict(df):
    return df[["business_id", "user_id", "stars"]]


# function removes multiple user reviews for same business
def remove_duplicates(df):
    df['date'] = pd.to_datetime(df['date'])
    dfsort = df.sort_values(by='date')
    df_filtered = dfsort.drop_duplicates(subset=['business_id', 'user_id'], keep='last')
    return df_filtered


# Creates a utility matrix for user ratings for businesses
# Arguments:
# df -- a dataFrame containing at least the columns 'userId' and 'businessId'
# Output:
# a matrix containing a rating in each cell. np.nan means that the user did not rate the business
def pivot_ratings(df):
    return df.pivot(values='stars', columns='user_id', index='business_id')


# calculates predicted rating
def predict_ids(similarity, utility, userId, itemId):
    # select right series from matrices and compute
    if userId in utility.columns and itemId in similarity.index:
        return predict_vectors(utility.loc[:, userId], similarity[itemId])
    return 0


def predict_vectors(user_ratings, similarities):
    # select only businesses actually rated by user
    relevant_ratings = user_ratings.dropna()

    # select corresponding similairties
    similarities_s = similarities[relevant_ratings.index]

    # select neighborhood
    similarities_s = similarities_s[similarities_s > 0.0]
    relevant_ratings = relevant_ratings[similarities_s.index]

    # if there's nothing left return a prediction of 0
    norm = similarities_s.sum()
    if (norm == 0):
        return 0

    # compute a weighted average (i.e. neighborhood is all)
    return np.dot(relevant_ratings, similarities_s) / norm


# Predicts the predicted rating for the input test data.
# Arguments:
# similarity -- a dataFrame that describes the similarity between items
# utility    -- a dataFrame that contains a rating for each user (columns) and each movie (rows).
#               If a user did not rate an item the value np.nan is assumed.
# to_predict -- A dataFrame containing at least the columns movieId and userId for which to do the predictions
def predict_ratings(similarity, utility, to_predict):
    # copy input (don't overwrite)
    ratings_test_c = to_predict.copy()
    # apply prediction to each row
    ratings_test_c['predicted rating'] = to_predict.apply(
        lambda row: predict_ids(similarity, utility, row['user_id'], row['business_id']), axis=1)
    return ratings_test_c


# returns all or n businesses with highest similarity
def predict(business_id, similarity_matrix, n=None):
    business = similarity_matrix[business_id].sort_values(ascending=False)
    if n:
        return business.head(n)
    return business

# returns a list that consists of (business_id, predicted rating), sorted by rating.
def predict_for_user(userId, reviews, similarity_matrix):
    reviews_filtered = reviews[reviews['user_id'] == userId]
    visited = reviews_filtered['business_id']
    ranking = {}
    for item in visited:
        try:
            predicted = predict(item, similarity_matrix, 10).to_dict()
            rating = reviews_filtered.loc[reviews_filtered['business_id'] == item, 'stars']
            rating = float(rating)
            for sim in predicted:
                if sim not in visited:
                    ranking[sim] = predicted[sim] * rating
        except:
            continue
    ranking = sorted(ranking.items(), key=lambda x: x[1], reverse=True)
    return ranking

# returns a list that consists of (business_id, predicted rating) by a weighted mean
def predict_user_weighted(userId, reviews, similarity_matrix):
    reviews_filtered = reviews[reviews['user_id'] == userId]
    reviews_un_filtered = reviews[reviews['user_id'] != userId]
    visited = reviews_filtered[['business_id', 'stars']]
    not_visited = reviews_un_filtered['business_id']

    rating_list = []
    for item in not_visited:
        try:
            df = predict(item, similarity_matrix)
            visited = visited.merge(df, left_on='business_id', right_on='business_id')
            visited['sim_star'] = visited['stars'] * visited[item]
            sum_star = sum(visited['sim_star'])
            sum_sim = sum(visited[item])
            rating_list.append(tuple((item, (sum_star/sum_sim), visited[item].max(), sum_sim, visited[item].max()/sum_sim)))
        except:
            continue

    return rating_list

