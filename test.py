from data import dataset, datasetdefined

# run dataset to determine the city yourself
dataset('las vegas', 'business', 10)
df = dataset('ajax', 'business', 'all')


# or use datasetdefined to use Las Vegas
datasetdefined('tip', 10)
datasetdefined('business', 'all')

print(df.loc[df['business_id'] == "zKtLlYDnjhoC5siq1A84gw", "name"].item())